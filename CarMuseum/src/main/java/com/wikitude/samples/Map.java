package com.wikitude.samples;

import android.os.Bundle;
import android.app.Activity;
import android.view.WindowManager;
import android.widget.ImageView;

import com.wikitude.samples.R;

public class Map extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().hide();

        //hide status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        String location = getIntent().getExtras().getString("location");
        ImageView mapImage = (ImageView) findViewById(R.id.map_image);

        switch(location){
            case "Fiat-600-Front":
            case "Fiat-600-Rear":
            case "BMW-Isetta-Side":
            case "BMW-Isetta-Front":
            case "BMW-Isetta-RightSide2":{
                mapImage.setImageResource(R.drawable.map);
                break;
            }
            default:{
                mapImage.setImageResource(R.drawable.map);
                break;
            }
        }
    }

}
