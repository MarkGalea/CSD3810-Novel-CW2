package com.wikitude.samples;

/**
 * Created by Mark on 02/03/2016.
 */
public class Delay {

    private long old;
    private long delayAmout;

    public Delay(long delayAmout) {
        this.old = System.currentTimeMillis();
        this.delayAmout = delayAmout;
    }

    /**
     * check if the time has expired
     * @param current
     * @return
     */
    public boolean check(long current){
        return (old + delayAmout <= current)? true : false;
    }

    /**
     * update the timer
     */
    public void update(){
        this.old = System.currentTimeMillis();
    }
}
