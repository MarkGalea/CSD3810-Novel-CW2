package com.wikitude.samples;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

public class ImageInfo extends Activity {

    public static final String EXTRAS_KEY_TARGETIMAGE = "targetImage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_info);
        getActionBar().hide();

        //hide status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        String targetImage = getIntent().getExtras().getString(EXTRAS_KEY_TARGETIMAGE);
        try {
            ((TextView) findViewById(R.id.targetImageLabel_1)).setText("Make & Model:");
            ((TextView) findViewById(R.id.targetImageLabel_2)).setText("Qty Produced:");
            ((TextView) findViewById(R.id.targetImageLabel_3)).setText("Year:");
            ((TextView) findViewById(R.id.targetImageLabel_4)).setText("Country:");
            ((TextView) findViewById(R.id.targetImageLabel_5)).setText("Engine Size:");
            ((TextView) findViewById(R.id.targetImageLabel_6)).setText("Transmission:");
            ((TextView) findViewById(R.id.targetImageLabel_7)).setText("Dimensions(LxWxH):");
            ((TextView) findViewById(R.id.targetImageLabel_8)).setText("Weight:");

            switch (targetImage) {
                case "Fiat-600-Front":
                case "Fiat-600-Rear": {
                    //Make & Model
                    ((TextView) findViewById(R.id.targetImage_1)).setText("Fiat 600");
                    //Qty Produced
                    ((TextView) findViewById(R.id.targetImage_2)).setText("4,921,626");
                    //Year
                    ((TextView) findViewById(R.id.targetImage_3)).setText("1955-1969");
                    //Country
                    ((TextView) findViewById(R.id.targetImage_4)).setText("Italy");
                    //Engine Size
                    ((TextView) findViewById(R.id.targetImage_5)).setText("633 cc - 843cc");
                    //Transmission
                    ((TextView) findViewById(R.id.targetImage_6)).setText("4-speed Manual");
                    //Dimensions(LxWxH)
                    ((TextView) findViewById(R.id.targetImage_7)).setText("3.2m x 1.4m x 1.4m");
                    //Weight
                    ((TextView) findViewById(R.id.targetImage_8)).setText("585 kg");
                    break;
                }

                case "BMW-Isetta-Side":
                case "BMW-Isetta-Front":
                case "BMW-Isetta-RightSide2": {
                    //Make & Model
                    ((TextView) findViewById(R.id.targetImage_1)).setText("Bmw Isetta");
                    //Qty Produced
                    ((TextView) findViewById(R.id.targetImage_2)).setText("161,360");
                    //Year
                    ((TextView) findViewById(R.id.targetImage_3)).setText("1955-1962");
                    //Country
                    ((TextView) findViewById(R.id.targetImage_4)).setText("Germany");
                    //Engine Size
                    ((TextView) findViewById(R.id.targetImage_5)).setText("298 cc");
                    //Transmission
                    ((TextView) findViewById(R.id.targetImage_6)).setText("4-speed Manual");
                    //Dimensions(LxWxH)
                    ((TextView) findViewById(R.id.targetImage_7)).setText("2.3m x 1.4m x 1.3m");
                    //Weight
                    ((TextView) findViewById(R.id.targetImage_8)).setText("580 kg");
                    break;
                }

                case "Ford-Escort-Front":{
                    //Make & Model
                    ((TextView) findViewById(R.id.targetImage_1)).setText("Ford Escort MK1");
                    //Qty Produced
                    ((TextView) findViewById(R.id.targetImage_2)).setText("134,424");
                    //Year
                    ((TextView) findViewById(R.id.targetImage_3)).setText("1968-1975");
                    //Country
                    ((TextView) findViewById(R.id.targetImage_4)).setText("North America");
                    //Engine Size
                    ((TextView) findViewById(R.id.targetImage_5)).setText("900 cc - 2000cc");
                    //Transmission
                    ((TextView) findViewById(R.id.targetImage_6)).setText("4-speed Manual or 3-speed Automatic");
                    //Dimensions(LxWxH)
                    ((TextView) findViewById(R.id.targetImage_7)).setText("4m x 1.6m x 1.5m");
                    //Weight
                    ((TextView) findViewById(R.id.targetImage_8)).setText("767 kg");
                    break;
                }

                case "Porsche-924-front":
                case "Porsche-924-interior": {
                    //Make & Model
                    ((TextView) findViewById(R.id.targetImage_1)).setText("Porsche 924");
                    //Qty Produced
                    ((TextView) findViewById(R.id.targetImage_2)).setText("242,568");
                    //Year
                    ((TextView) findViewById(R.id.targetImage_3)).setText("1976–1988");
                    //Country
                    ((TextView) findViewById(R.id.targetImage_4)).setText("Germany");
                    //Engine Size
                    ((TextView) findViewById(R.id.targetImage_5)).setText("2000 cc or 2500 cc");
                    //Transmission
                    ((TextView) findViewById(R.id.targetImage_6)).setText("4-speed Manual");
                    //Dimensions(LxWxH)
                    ((TextView) findViewById(R.id.targetImage_7)).setText("4.2m x 1.7m x 1.3m");
                    //Weight
                    ((TextView) findViewById(R.id.targetImage_8)).setText("1,080 kg");
                    break;
                }

                case "Fiat-500-interior":
                case "Fiat-500D-Side": {
                    //Make & Model
                    ((TextView) findViewById(R.id.targetImage_1)).setText("Fiat 500D");
                    //Qty Produced
                    ((TextView) findViewById(R.id.targetImage_2)).setText("640,520");
                    //Year
                    ((TextView) findViewById(R.id.targetImage_3)).setText("1960–1965");
                    //Country
                    ((TextView) findViewById(R.id.targetImage_4)).setText("Italy");
                    //Engine Size
                    ((TextView) findViewById(R.id.targetImage_5)).setText("500cc");
                    //Transmission
                    ((TextView) findViewById(R.id.targetImage_6)).setText("4-speed Manual");
                    //Dimensions(LxWxH)
                    ((TextView) findViewById(R.id.targetImage_7)).setText("3m x 1.3m x 1.3m");
                    //Weight
                    ((TextView) findViewById(R.id.targetImage_8)).setText("500 kg");
                    break;
                }

                case "Fiat-500-front":
                case "fiat-500-rear": {
                    //Make & Model
                    ((TextView) findViewById(R.id.targetImage_1)).setText("Fiat 500 Abarth");
                    //Qty Produced
                    ((TextView) findViewById(R.id.targetImage_2)).setText("About 50,000");
                    //Year
                    ((TextView) findViewById(R.id.targetImage_3)).setText("1960–1970");
                    //Country
                    ((TextView) findViewById(R.id.targetImage_4)).setText("Italy");
                    //Engine Size
                    ((TextView) findViewById(R.id.targetImage_5)).setText("595cc");
                    //Transmission
                    ((TextView) findViewById(R.id.targetImage_6)).setText("4-speed Manual");
                    //Dimensions(LxWxH)
                    ((TextView) findViewById(R.id.targetImage_7)).setText("3m x 1.3m x 1.3m");
                    //Weight
                    ((TextView) findViewById(R.id.targetImage_8)).setText("570 kg");
                    break;
                }

                default: {
                    //Make & Model
                    ((TextView) findViewById(R.id.targetImage_1)).setText("N/A");
                    //Qty Produced
                    ((TextView) findViewById(R.id.targetImage_2)).setText("N/A");
                    //Year
                    ((TextView) findViewById(R.id.targetImage_3)).setText("N/A");
                    //Country
                    ((TextView) findViewById(R.id.targetImage_4)).setText("N/A");
                    //Engine Size
                    ((TextView) findViewById(R.id.targetImage_5)).setText("N/A");
                    //Transmission
                    ((TextView) findViewById(R.id.targetImage_6)).setText("N/A");
                    //Dimensions(LxWxH)
                    ((TextView) findViewById(R.id.targetImage_7)).setText("N/A");
                    //Weight
                    ((TextView) findViewById(R.id.targetImage_8)).setText("N/A");
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
