package com.wikitude.samples;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class Discount extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);
        getActionBar().hide();

        //hide status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        int discountCode = getIntent().getExtras().getInt("discount");

        ImageView mapImage = (ImageView) findViewById(R.id.discount_qr);

        //temp
        //TextView test = (TextView) findViewById(R.id.discount_qr_text);
        //test.setText("" + discountCode);

        switch(discountCode){
            case 1:{
                mapImage.setImageResource(R.drawable.qr_1);
                break;
            }
            case 2:{
                mapImage.setImageResource(R.drawable.qr_2);
                break;
            }
            case 3:{
                mapImage.setImageResource(R.drawable.qr_3);
                break;
            }
            case 4:{
                mapImage.setImageResource(R.drawable.qr_4);
                break;
            }
            case 5:{
                mapImage.setImageResource(R.drawable.qr_5);
                break;
            }
            case 6:{
                mapImage.setImageResource(R.drawable.qr_6);
                break;
            }
            case 7:{
                mapImage.setImageResource(R.drawable.qr_7);
                break;
            }
            case 8:{
                mapImage.setImageResource(R.drawable.qr_8);
                break;
            }
            case 9:{
                mapImage.setImageResource(R.drawable.qr_9);
                break;
            }
            case 10:{
                mapImage.setImageResource(R.drawable.qr_10);
                break;
            }
            case 11:
            default:{
                mapImage.setImageResource(R.drawable.qr_11);
                break;
            }
        }
    }
}
