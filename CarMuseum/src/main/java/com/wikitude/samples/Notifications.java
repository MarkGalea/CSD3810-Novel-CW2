package com.wikitude.samples;

import com.wikitude.architect.ArchitectView;

/**
 * Created by Mark on 04/03/2016.
 */
public class Notifications {

    public static void pushGameToggleNotification(ArchitectView architectView, boolean isOn){
        if(isOn){
            architectView.callJavascript("World.gameInstructions('gameModeOn');");
        }else{
            architectView.callJavascript("World.gameInstructions('gameModeOff');");
        }
    }

    public static void pushDiscountNotification(ArchitectView architectView){
        architectView.callJavascript("World.error('A 5% discount at the Cafeteria is given upon completing the game!',3, true);");
    }

}
