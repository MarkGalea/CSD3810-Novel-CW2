var World = {
        loaded: false,

        /* Cars */
        fiat600: new Car("Fiat 600", "1955-1969", "Italy", "Fiat-600-Front", "Fiat-600-Rear", "", "", "") ,
        bmwIsetta: new Car("Bmw Isetta", "1955-1962", "Germany", "BMW-Isetta-Front", "", "", "BMW-Isetta-Side", ""),
        fordEscortMk1: new Car("Ford Escort MK1", "1968–1975", "North America", "Ford-Escort-Front", "", "", "", ""),
        porsche924: new Car("Porsche 924", "1976–1988", "Germany", "Porsche-924-front", "", "Porsche-924-interior", "", ""),
        fiat500D: new Car("Fiat 500D", "1960–1965", "Italy", "", "", "Fiat-500-interior", "", "Fiat-500D-Side"),
        fiat500Abarth: new Car("Fiat 500 Abarth", "1962", "Italy", "Fiat-500-front", "fiat-500-rear", "", "", ""),

        //3d model
        rotating: false,
        trackableVisible: false,
        snapped: false,
        lastTouch: {
                x: 0,
                y: 0
        },
        lastScale: 0,
        currentScale: 0,
        swipeAllowed: true,
        interactionContainer: 'snapContainer',
        layout: {
                normal: {
                        offsetX: 0.35,
                        offsetY: 0.45,
                        opacity: 0.0,
                        carScale: 0.1,
                        carTranslateY: 0
                },
                snapped: {
                        offsetX: 0.45,
                        offsetY: 0.45,
                        opacity: 0.2,
                        carScale: 0.1,
                        carTranslateY: 0.0
                }
        },

        fiat5003DModel: null,

        //Game Variables
        gameStepWizard: 0,
        gameMode: false,

        //Notifications
        messagesSetUp: false,

        //trackable objects
        trackableObjects: null,

        init: function initFn() {
                this.createOverlays();
        },


        createOverlays: function createOverlaysFn() {


                /*
                	Initliaze tracker file
                */
                this.tracker = new AR.ClientTracker("assets/targetmanager/classiccarmuseum.wtc", {
                        onLoaded: this.loadingStep
                });

                /******* Add any image resources required *******/
                // Create play button which is used for starting the video
                var playButtonImgResource = new AR.ImageResource("assets/icons/playButton.png");
                var img3DRotate = new AR.ImageResource("assets/icons/rotateButton.png");
                var img3DSnap = new AR.ImageResource("assets/icons/snapButton.png");
                /***** Car Media *****/
                World.trackableObjects = [];

                //Add image overlays (tracker, imageLocation, targetName, size, offsetX, offsetY)
                World.addImage(this.tracker, "assets/cars/fiat600/Fiat-600-Boot.jpg", World.fiat600.getFrontPictureName(), 0.15, 0, 0);
                World.addImage(this.tracker, "assets/cars/bmwisetta/Bmw-Isetta-Interior.jpg", World.bmwIsetta.getFrontPictureName(), 0.225, -0.10, -0.01);
                World.addImage(this.tracker, "assets/cars/fordescortmk1/ford-escort-engine.jpg", World.fordEscortMk1.getFrontPictureName(), 0.20, -0.08, 0.02);
                World.addImage(this.tracker, "assets/cars/fiat500abarth/fiat-500-front.png", World.fiat500Abarth.getFrontPictureName(), 0.25, 0.05, 0);

                //Add video overlays ((tracker, videoLocation, targetName, playButtonImg, size, offSetY, offSetX))
                World.addVideo(this.tracker, "assets/cars/fiat600/Fiat-600-Engine-Running.mp4", World.fiat600.getRearPictureName(), playButtonImgResource, 0.325, -0.10, 0);
                World.addVideo(this.tracker, "assets/cars/bmwisetta/Bmw-Isetta-Engine-Running.mp4", World.bmwIsetta.getRightSidePictureName(), playButtonImgResource, 0.25, -0.18, -0.15);
                World.addVideo(this.tracker, "assets/cars/porsche924/Porsche-924-EngineStart.mp4", World.porsche924.getFrontPictureName(), playButtonImgResource, 0.30, 0.02, -0.05);
                World.addVideo(this.tracker, "assets/cars/porsche924/Porsche-924-Interior-Racing.mp4", World.porsche924.getInteriorPictureName(), playButtonImgResource, 0.85, -0.05, 0.20);
                World.addVideo(this.tracker, "assets/cars/fiat500D/fiat-500-drive.mp4", World.fiat500D.getInteriorPictureName(), playButtonImgResource, 0.65, 0, 0);
                World.addVideo(this.tracker, "assets/cars/fiat500abarth/fiat-500-engine.mp4", World.fiat500Abarth.getRearPictureName(), playButtonImgResource, 0.30, -0.10, 0.20);

                //Adding the 3d Models
                World.fiat5003DModel = new AR.Model("assets/cars/fiat500D/fiat-500-model.wt3", {
                        onLoaded: this.loadingStep,
                        scale: {
                                x: 0.0,
                                y: 0.0,
                                z: 0.0
                        },
                        translate: {
                                x: 0.0,
                                y: 0.05,
                                z: 0.0
                        },
                        rotate: {
                                roll: -25
                        }
                });

                //Adding the rotate model button the 3d state
                this.buttonRotate = new AR.ImageDrawable(img3DRotate, 0.2, {
                        offsetX: 0.35,
                        offsetY: 0.45,
                        onClick: this.toggleAnimateModel
                });
                //Adding the snap model button the 3d state
                this.buttonSnap = new AR.ImageDrawable(img3DSnap, 0.2, {
                        offsetX: -0.35,
                        offsetY: -0.45,
                        onClick: this.toggleSnapping
                });
                //Adding an appearing animation to the 3d model
                this.appearingAnimation = this.createAppearingAnimation(World.fiat5003DModel, 0.045);
                //Adding the rotating animation when the rotate button is pressed
                this.rotationAnimation = new AR.PropertyAnimation(World.fiat5003DModel, "rotate.roll", -25, 335, 10000);

                //Tracking all the recognized targets and adding the info page to each target
                new AR.Trackable2DObject(this.tracker, "*", {
                        //When a target is reconigized
                        onEnterFieldOfVision: function(targetName) {
                                var targetNameStr = encodeURIComponent(targetName).trim();

                                //update native location for map
                                document.location = "architectsdk://recognizedTargets?targetName=" + targetNameStr;

                                //display the info button
                                document.getElementById('infoButton').style.display = "block";

                                //game functionality
                                World.game(targetName);
                                if (World.gameMode == false) {
                                        var carInfo = "";
                                        switch (targetNameStr) {
                                                case "Fiat-600-Front":
                                                case "Fiat-600-Rear":
                                                        carInfo = "Model: " + World.fiat600.getName() + ", Years: " + World.fiat600.getAge() + " and Country: " + World.fiat600.getCountry();
                                                        World.message(carInfo, 0, true);
                                                        break;
                                                case "BMW-Isetta-Front":
                                                case "BMW-Isetta-Side":
                                                        carInfo = "Model: " + World.bmwIsetta.getName() + ", Years: " + World.bmwIsetta.getAge() + " and Country: " + World.bmwIsetta.getCountry();
                                                        World.message(carInfo, 0, true);
                                                        break;
                                                case "Ford-Escort-Front":
                                                        carInfo = "Model: " + World.fordEscortMk1.getName() + ", Years: " + World.fordEscortMk1.getAge() + "and Country: " + World.fordEscortMk1.getCountry();
                                                        World.message(carInfo, 0, true);
                                                        break;
                                                case "Porsche-924-front":
                                                case "Porsche-924-interior":
                                                        carInfo = "Model: " + World.fordEscortMk1.getName() + ", Years: " + World.fordEscortMk1.getAge() + " and Country: " + World.fordEscortMk1.getCountry();
                                                        World.message(carInfo, 0, true);
                                                        break;
                                                case "Fiat-500-interior":
                                                case "Fiat-500D-Side":
                                                        carInfo = "Model: " + World.fiat500D.getName() + ", Years: " + World.fiat500D.getAge() + " and Country: " + World.fiat500D.getCountry();
                                                        World.message(carInfo, 0, true);
                                                        break;
                                                case "Fiat-500-front":
                                                case "fiat-500-rear":
                                                        carInfo = "Model: " + World.fiat500Abarth.getName() + ", Years: " + World.fiat500Abarth.getAge() + " and Country: " + World.fiat500Abarth.getCountry();
                                                        World.message(carInfo, 0, true);
                                                        break;
                                                default:
                                                        World.message("Sorry, this vehicle doesn't have AR capabilities.", 0, true);
                                                        break;
                                        }
                                }
                        },
                        //When no target is reconigized
                        onExitFieldOfVision: function() {
                                alertify.dismissAll();
                                document.getElementById('infoButton').style.display = "none";
                        }
                });

                //Adding a container to the reconigzed target
                this.trackable = new AR.Trackable2DObject(this.tracker, World.fiat500D.getLeftSidePictureName(), {
                        drawables: {
                                cam: [World.fiat5003DModel, this.buttonRotate, this.buttonSnap]
                        },
                        snapToScreen: {
                                snapContainer: document.getElementById('snapContainer')
                        },
                        onEnterFieldOfVision: this.appear,
                        onExitFieldOfVision: this.disappear
                });


                /*
                	Event handler for touch and gesture events for the 3D model.
                */
                this.handleTouchStart = function handleTouchStartFn(event) {

                                World.swipeAllowed = true;

                                /* Once a new touch cycle starts, keep a save it's originating location */
                                World.lastTouch.x = event.touches[0].clientX;
                                World.lastTouch.y = event.touches[0].clientY;

                                event.preventDefault();
                        },

                        this.handleTouchMove = function handleTouchMoveFn(event) {

                                if (World.swipeAllowed) {
                                        /* Define some local variables to keep track of the new touch location and the 
                                        movement between the last event and the current one */
                                        var touch = {
                                                x: event.touches[0].clientX,
                                                y: event.touches[0].clientY
                                        };
                                        var movement = {
                                                x: 0,
                                                y: 0
                                        };
                                        /* Calculate the touch movement between this event and the last one */
                                        movement.x = (World.lastTouch.x - touch.x) * -1;
                                        movement.y = (World.lastTouch.y - touch.y) * -1;
                                        /* Rotate the car model accordingly to the calculated movement values.
                                           Note: we're slowing the movement down so that the touch action feels better*/
                                        World.fiat5003DModel.rotate.roll += (movement.x * 0.3);
                                        World.fiat5003DModel.rotate.tilt += (movement.y * 0.3);
                                        /* Keep track of the current touch location. We need them in the next move cycle */
                                        World.lastTouch.x = touch.x;
                                        World.lastTouch.y = touch.y;
                                }
                                event.preventDefault();
                        },

                        this.handleGestureStart = function handleGestureStartFn(event) {
                                /* Once a gesture is recognized, disable rotation changes */
                                World.swipeAllowed = false;
                                World.lastScale = event.scale;
                        },

                        this.handleGestureChange = function handleGestureChangeFn(event) {
                                /* Calculate the new scaling delta that should applied to the 3D model. */
                                var deltaScale = (event.scale - World.lastScale) * 0.1;
                                /* Negative scale values are not allowd by the 3D model API. So we use the Math.max 
                                function to ensure scale values >= 0. */
                                var newScale = Math.max(World.fiat5003DModel.scale.x + deltaScale, 0);
                                World.fiat5003DModel.scale = {
                                        x: newScale,
                                        y: newScale,
                                        z: newScale
                                };
                                /* Keep track of the current scale value so that we can calculate the scale delta in 
                                the next gesture changed function call */
                                World.lastScale = event.scale;
                        },
                        this.handleGestureEnd = function handleGestureEndFn(event) {
                                /* Once the gesture ends, allow rotation changes again */
                                World.swipeAllowed = true;
                                World.lastScale = event.scale;
                        }
        },

        //Loading the 3d Model
        loadingStep: function loadingStepFn() {
                if (!World.loaded && World.tracker.isLoaded() && World.fiat5003DModel.isLoaded()) {
                        World.loaded = true;

                        if (World.trackableVisible && !World.appearingAnimation.isRunning()) {
                                World.appearingAnimation.start();
                        }
                }
        },

        //Add Video Overlays
        addVideo: function addVideoFn(tracker, videoLocation, targetName, playButtonImg, size, offSetY, offSetX) {

                var playButton = new AR.ImageDrawable(playButtonImg, size, {
                        enabled: false,
                        clicked: false,
                        onClick: function playButtonClicked() {
                                video.play(1);
                                video.playing = true;
                                playButton.clicked = true;
                        },
                        offsetY: offSetY,
                        offsetX: offSetX
                });

                var video = new AR.VideoDrawable(videoLocation, size, {
                        offsetY: playButton.offsetY,
                        offsetX: playButton.offsetX,
                        onLoaded: function videoLoaded() {
                                playButton.enabled = true;
                        },
                        onPlaybackStarted: function videoPlaying() {
                                playButton.enabled = false;
                                video.enabled = true;
                        },
                        onFinishedPlaying: function videoFinished() {
                                playButton.enabled = true;
                                video.playing = false;
                                video.enabled = false;
                        },
                        onClick: function videoClicked() {
                                if (playButton.clicked) {
                                        playButton.clicked = false;
                                } else if (video.playing) {
                                        video.pause();
                                        video.playing = false;
                                } else {
                                        video.resume();
                                        video.playing = true;
                                }
                        }
                });

                var videoAr = new AR.Trackable2DObject(tracker, targetName, {
                        drawables: {
                                cam: [video, playButton]
                        },
                        onEnterFieldOfVision: function onEnterFieldOfVisionFn() {
                                if (video.playing && World.gameMode == false) {
                                        video.resume();
                                }
                        },
                        onExitFieldOfVision: function onExitFieldOfVisionFn() {
                                if (video.playing) {
                                        video.pause();
                                }
                        }
                });
                World.trackableObjects.push(videoAr);
                return videoAr;
        },

        // Add Image Overlays
        addImage: function addImageFn(tracker, imageLocation, targetName, size, offsetY, offsetX) { 
                var image = new AR.ImageResource(imageLocation); 
                var overlay = new AR.ImageDrawable(image, size, { 
                        offsetX: offsetX,
                         
                        offsetY: offsetY 
                });  
                var imageAR = new AR.Trackable2DObject(tracker, targetName, { 
                        drawables: { 
                                cam: overlay 
                        } 
                }); 
                World.trackableObjects.push(imageAR);
                return imageAR; 
        },
         
        //Creating the 3D model Apperaing Animation
        createAppearingAnimation: function createAppearingAnimationFn(model, scale) {
                /*
                	The animation scales up the 3D model once the target is inside the field of vision.
                	Creating an animation on a single property of an object is done using an
                	AR.PropertyAnimation. Since the car model needs to be scaled up on all three axis,
                	three animations are needed. These animations are grouped together utilizing an
                	AR.AnimationGroup that allows them to play them in parallel.
                	Each AR.PropertyAnimation targets one of the three axis and scales the model from 0 to
                	the value passed in the scale variable. An easing curve is used to create a more dynamic
                	 effect of the animation.
                */
                var sx = new AR.PropertyAnimation(model, "scale.x", 0, scale, 1500, {
                        type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
                });
                var sy = new AR.PropertyAnimation(model, "scale.y", 0, scale, 1500, {
                        type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
                });
                var sz = new AR.PropertyAnimation(model, "scale.z", 0, scale, 1500, {
                        type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
                });
                return new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, [sx, sy, sz]);
        },

        appear: function appearFn() {
                World.trackableVisible = true;
                if (World.loaded && !World.snapped) {
                        // Resets the properties to the initial values.
                        World.resetModel();
                        World.appearingAnimation.start();
                }
        },
        disappear: function disappearFn() {
                World.trackableVisible = false;
        },

        resetModel: function resetModelFn() {
                World.rotationAnimation.stop();
                World.rotating = false;
                World.fiat5003DModel.rotate.roll = -25;
        },

        toggleAnimateModel: function toggleAnimateModelFn() {
                if (!World.rotationAnimation.isRunning()) {
                        if (!World.rotating) {
                                // Starting an animation with .start(-1) will loop it indefinitely.
                                World.rotationAnimation.start(-1);
                                World.rotating = true;
                        } else {
                                // Resumes the rotation animation
                                World.rotationAnimation.resume();
                        }
                } else {
                        // Pauses the rotation animation
                        World.rotationAnimation.pause();
                }

                return false;
        },

        /*
        	This function is used to either snap the trackable onto the screen or to detach it.
        */
        toggleSnapping: function toggleSnappingFn() {

                World.snapped = !World.snapped;
                World.trackable.snapToScreen.enabled = World.snapped;

                if (World.snapped) {

                        World.applyLayout(World.layout.snapped);

                        World.addInteractionEventListener();

                } else {

                        World.applyLayout(World.layout.normal);

                        World.removeInteractionEventListener();
                }
        },

        /*
        	applyLayout is used to define position and scale of certain drawables in the scene for
        	certain states. The different layouts are defined at the top of the World object.
        */
        applyLayout: function applyLayoutFn(layout) {

                World.buttonRotate.offsetX = layout.offsetX;
                World.buttonRotate.offsetY = layout.offsetY;

                World.buttonSnap.offsetX = -layout.offsetX;
                World.buttonSnap.offsetY = -layout.offsetY;


                World.fiat5003DModel.scale = {
                        x: layout.carScale,
                        y: layout.carScale,
                        z: layout.carScale
                };
                World.fiat5003DModel.translate = {
                        x: 0.0,
                        y: layout.carTranslateY,
                        z: 0.0
                };
                document.getElementById(World.interactionContainer).style.opacity = layout.opacity.toString();
        },

        /*
        	Touch and gesture listener are added to allow rotation and scale changes in the snappedto screen state.
        */
        addInteractionEventListener: function addInteractionEventListenerFn() {
                document.getElementById(World.interactionContainer).addEventListener('touchstart', World.handleTouchStart, false);
                document.getElementById(World.interactionContainer).addEventListener('touchmove', World.handleTouchMove, false);

                document.getElementById(World.interactionContainer).addEventListener('gesturestart', World.handleGestureStart, false);
                document.getElementById(World.interactionContainer).addEventListener('gesturechange', World.handleGestureChange, false);
                document.getElementById(World.interactionContainer).addEventListener('gestureend', World.handleGestureEnd, false);
        },

        removeInteractionEventListener: function removeInteractionEventListenerFn() {
                document.getElementById(World.interactionContainer).removeEventListener('touchstart', World.handleTouchStart, false);
                document.getElementById(World.interactionContainer).removeEventListener('touchmove', World.handleTouchMove, false);

                document.getElementById(World.interactionContainer).removeEventListener('gesturestart', World.handleGestureStart, false);
                document.getElementById(World.interactionContainer).removeEventListener('gesturechange', World.handleGestureChange, false);
                document.getElementById(World.interactionContainer).removeEventListener('gestureend', World.handleGestureEnd, false);
        },

        /******** Notification System ********/

        alertSystem: function alertSystemFn(removePreviousMessages) {
                if (World.messagesSetUp == false) {
                        alertify.set('notifier', 'position', 'top-right');
                        World.messagesSetUp = true;
                }
                if (removePreviousMessages == true) {
                        alertify.dismissAll();
                }
        },

        message: function messageFn(message, delay, removePreviousMessages) {
                World.alertSystem(removePreviousMessages);
                alertify.message(message, delay);
        },

        error: function errorFn(message, delay, removePreviousMessages) {
                World.alertSystem(removePreviousMessages);
                alertify.error(message, delay);
        },

        warning: function warningFn(message, delay, removePreviousMessages) {
                World.alertSystem(removePreviousMessages);
                alertify.warning(message, delay);
        },

        success: function successFn(message, delay, removePreviousMessages) {
                World.alertSystem(removePreviousMessages);
                alertify.success(message, delay);
        },

        /******** Game ********/
        gameInstructions: function gameInstructionsFn(stage) {
                switch (stage) {
                        case "gameModeOn":
                                World.gameMode = true; 
                                World.message("Game Mode has been turned on!", 2, true);
                                setTimeout(function() {
                                        World.gameShowMessageBasedOnGameStepWizard();
                                        World.enableTrackableObjects(false);
                                }, 2000);
                                break;
                        case "gameModeOff":
                                World.enableTrackableObjects(true);
                                World.gameMode = false; 
                                World.message("Game Mode has been turned off!", 2, true);
                                break;
                        case "findFiat600":
                                World.message("Find the Fiat 600", 0, true);
                                break;
                        case "findFordEscort":
                                World.message("Find the Ford Escort Mk1", 0, true);
                                break;
                        case "findBmwIsetta":
                                World.message("Find the Bmw Isetta", 0, true);
                                break;
                        case "findTheEngine":
                                World.message("Find the car's engine", 0, true);
                                break;
                        case "findTheFuelTank":
                                World.message("Find the car's fuel tank", 0, true);
                                break;
                        case "invalid":
                                World.warning("Invalid Target!", 1.5, true);
                                setTimeout(function() {
                                        World.gameShowMessageBasedOnGameStepWizard();
                                }, 1500);
                                break;
                        case "correct":
                                World.success("Correct!", 3, true);
                                setTimeout(function() {
                                        World.gameShowMessageBasedOnGameStepWizard();
                                }, 1500);
                                break;
                        case "gameCompleted":
                                World.enableTrackableObjects(true);
                                World.success("Game Completed!", 2, true);
                                World.success("Reedem your discount code!", 3, false);
                                break;
                        case "discount":
                                if (World.gameMode == true) { 
                                        World.gameShowMessageBasedOnGameStepWizard(); 
                                } else {
                                        World.warning("The 5% discount at the Cafeteria is given upon completing the game!", 3, false);
                                }
                                break;
                        default:
                                break;
                }
        },

        //reset game and send message to native to uncheck the gamemode button
        resetGame: function resetGameFn() {
                setTimeout(function() {
                        World.gameStepWizard = 0;
                        World.gameMode = false;
                        document.location = "architectsdk://gameCompleted";
                }, 2000);
        },

        /*  method used to show the message for the current step, if more steps are added they must be
         added here too */
        gameShowMessageBasedOnGameStepWizard: function gameShowMessageBasedOnGameStepWizardFn() {
                switch (World.gameStepWizard) {
                        case 0:
                                World.gameInstructions("findFiat600");
                                break;
                        case 1:
                                World.gameInstructions("findTheEngine");
                                break;
                        case 2:
                                World.gameInstructions("findTheFuelTank");
                                break;
                        case 3:
                                World.gameInstructions("findBmwIsetta");
                                break;
                        case 4:
                                World.gameInstructions("findTheEngine");
                                break;
                        case 5:
                                World.gameInstructions("gameCompleted");
                                break;
                        default:
                                break;
                }
        },

        //game core functionality. checks the step wizard
        game: function gameFn(targetName) {
                if (World.gameMode == true) {
                        switch (World.gameStepWizard) {
                                case 0:
                                        World.gameHelperFunctionality(targetName, World.fiat600.getFrontPictureName(), "findFiat600");
                                        break;
                                case 1:
                                        World.gameHelperFunctionality(targetName, World.fiat600.getRearPictureName(), "findTheEngine");
                                        break;
                                case 2:
                                        World.gameHelperFunctionality(targetName, World.fiat600.getFrontPictureName(), "findTheFuelTank");
                                        break;
                                case 3:
                                        World.gameHelperFunctionality(targetName, World.bmwIsetta.getFrontPictureName(), "findBmwIsetta");
                                        break;
                                case 4:
                                        World.gameHelperFunctionality(targetName, World.bmwIsetta.getRightSidePictureName(), "findTheEngine");
                                        break;
                                case 5:
                                        World.resetGame();
                                        break;
                                default:
                                        break;
                        }
                        World.gameInstructions(stage);
                }
        },

        //method used to check if the image is correct and send messages accordingly
        gameHelperFunctionality: function gameHelperFunctionalityFn(targetName, expectedTargetName, gameBannerName) {
                //target found
                if (targetName == expectedTargetName) {
                        World.gameInstructions("correct");
                        World.gameStepWizard = World.gameStepWizard + 1;
                        return true;
                }
                //target not found
                else {
                        World.gameInstructions("invalid");
                }
                return false;
        },

        enableTrackableObjects: function enableTrackableObjectsFn(enable) {
                if (enable == false) {
                        for (i = 0; World.trackableObjects.length; i++) {
                                World.trackableObjects[i].enabled = false;
                        }
                } else {
                        for (i = 0; World.trackableObjects.length; i++) {
                                World.trackableObjects[i].enabled = true;
                        }
                }
        }
};

World.init();

function nativeInfo() {
        document.location = "architectsdk://getImageInfo";
}