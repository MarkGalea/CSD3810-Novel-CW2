function Car(name, age, country, frontPictureName, rearPictureName, interiorPictureName, rightSidePictureName, leftSidePictureName) {
    this.name = name;
    this.age = age;
    this.country = country;
    this.frontPictureName = frontPictureName;
    this.rearPictureName = rearPictureName;
    this.interiorPictureName = interiorPictureName;
    this.rightSidePictureName = rightSidePictureName;
    this.leftSidePictureName = leftSidePictureName;
}

Car.prototype.getName = function() {
    return this.name;
};

Car.prototype.getAge = function() {
    return this.age;
};

Car.prototype.getCountry = function() {
    return this.country;
};

Car.prototype.getFrontPictureName = function() {
    return this.frontPictureName;
};

Car.prototype.getRearPictureName = function() {
    return this.rearPictureName;
};

Car.prototype.getInteriorPictureName = function() {
    return this.interiorPictureName;
};

Car.prototype.getRightSidePictureName = function() {
    return this.rightSidePictureName;
};

Car.prototype.getLeftSidePictureName = function() {
    return this.leftSidePictureName;
};